<?php ?>
<div class="watchlist-wrapper">
    <div class="graph-wrapper">
        <h2 id="graph-heading">Deine Performance</h2>
        <img src="./assets/img/chart_placeholder.png" alt="hier sollte ein Bild stehen" class="performance-graph"/>
    </div>
    <div id="switch-buttons-wrapper">
        <button class="switch-button active" id="watchlist-button">Watchlist</button>
        <button class="switch-button" id="portfolio-button">Portfolio</button>
    </div>
    <div id="watchlist-table-wrapper">
        <table id="watchlist-table">
            <thead class="watchlist-table-head">
            <tr>
                <th>
                    Ticker
                </th>
                <th>
                    Price
                </th>
                <th>
                    Change %
                </th>
                <th>
                    Change $
                </th>
                <th>
                    Exchange
                </th>
            </tr>
            </thead>
            <tbody class="watchlist-table-body">
            <tr id="watchlist-first-row">
                <td>
                    <button type="button" class="button-add-stock" id="watchlist-button-add-stock">+</button>
                </td>
                <td>

                </td>
                <td>

                </td>
                <td>

                </td>
                <td>

                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div id="portfolio-table-wrapper">
        <table id="portfolio-table">
            <thead class="portfolio-table-head">
            <tr>
                <th>
                    Ticker
                </th>
                <th>
                    Price
                </th>
                <th>
                    Change %
                </th>
                <th>
                    Change $
                </th>
                <th>
                    Exchange
                </th>
            </tr>
            </thead>
            <tbody class="portfolio-table-body">
            <tr>
                <td>
                    test
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
