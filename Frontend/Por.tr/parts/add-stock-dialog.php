<?php ?>
<div id="add-stock-dialog-wrapper">
    <div class="add-stock-dialog">
        <div class="heading">
            <h2>Aktie hinzufügen</h2>
        </div>
        <form class="add-stock-dialog-center">
            <label for="kaufpreis">Kaufpreis</label>
            <input type="number" name="kaufpreis" id="kaufpreis" required>
            <label for="kaufdatum">Kaufdatum</label>
            <input type="date" name="kaufdatum" id="kaufdatum" required>
            <label for="testfeld">Testfeld</label>
            <input type="text" name="testfeld" id="testfeld">
            <div class="add-stock-dialog-bottom">
                <input type="submit" id="add-stock-confirm-button" value="Zur Watchlist hinzufügen">
            </div>
        </form>

    </div>
</div>
