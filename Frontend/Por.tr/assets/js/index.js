window.addEventListener('DOMContentLoaded', (e) => {
        const D = document;


        let switchButtons = document.getElementsByClassName('switch-button')
        let addStockConfirmButton = D.getElementById('add-stock-confirm-button')

        for (let i = 0; i < switchButtons.length; i++) {
            switchButtons[i].addEventListener("click", assignActive)
        }

        function assignActive() {
            let current = document.getElementsByClassName("active");
            console.log(current[0].className)
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        }

        for (let i = 0; i < switchButtons.length; i++) {
            switchButtons[i].addEventListener("click", checkActive)
        }

        function checkActive() {
            let watchlistTableWrapper = document.getElementById('watchlist-table-wrapper')
            let portfolioTableWrapper = document.getElementById('portfolio-table-wrapper')
            if (switchButtons[0].className.includes('active')) {
                portfolioTableWrapper.style.display = 'none';
                watchlistTableWrapper.style.display = 'initial'
            } else if (switchButtons[1].className.includes('active')) {
                watchlistTableWrapper.style.display = 'none';
                portfolioTableWrapper.style.display = 'initial';
            }
        }

        let addStockButton = document.getElementById('watchlist-button-add-stock')
        let addStockDialog = document.getElementById('add-stock-dialog-wrapper')

        function addStockClick() {
            addStockDialog.style.display = 'block'
        }

        addStockButton.addEventListener('click', addStockClick);

        let watchlistTable = D.getElementById('watchlist-table')
        console.log(watchlistTable)

        function addStockConfirm() {
            let kaufpreis = D.getElementById('kaufpreis')
            let kaufdatum = D.getElementById('kaufdatum')
            let testfeld = D.getElementById('testfeld')

            let watchlistFirstRow = D.getElementById('watchlist-first-row')
            let watchlistTableNewRow = watchlistTable.insertRow();
            let newCellsArray = watchlistTableNewRow.getElementsByTagName('td')
            let newCell;
            let numberOfWatchlistCells = watchlistFirstRow.getElementsByTagName('td').length

            for (let i = 0; i < numberOfWatchlistCells; i++) {
                newCell = watchlistTableNewRow.insertCell(0);
                console.log(i)
            }

            watchlistFirstRow.before(watchlistTableNewRow);

            if (!(kaufpreis.value === '') && !(kaufdatum.value === '')) {
                addStockDialog.style.display = 'none'
            }
        }

        addStockConfirmButton.addEventListener('click', addStockConfirm)

    }
)
