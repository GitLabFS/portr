import requests
import json

#API auto-complete

url = "https://yh-finance.p.rapidapi.com/auto-complete"

querystring = {"q":"TSLA","region":"US"}

headers = {
	"X-RapidAPI-Host": "yh-finance.p.rapidapi.com",
	"X-RapidAPI-Key": "17a3e864dfmsh930ed9965b132b9p1c5c70jsn9fdc751b8883"
}

response = requests.request("GET", url, headers=headers, params=querystring)
auto_complete = response.json()

with open("auto_complete.json", "w") as f:
    f.write(json.dumps(auto_complete, indent= 4))

#quotes_list = testdatei["quotes"]
#for qoutes in quotes_list:
 #   if qoutes["symbol"] == "TSLA":
  #      print(qoutes["exchange"])






#API get-summary

url = "https://yh-finance.p.rapidapi.com/stock/v2/get-summary"

querystring = {"symbol":"AMRN","region":"US"}

headers = {
	"X-RapidAPI-Host": "yh-finance.p.rapidapi.com",
	"X-RapidAPI-Key": "17a3e864dfmsh930ed9965b132b9p1c5c70jsn9fdc751b8883"
}

response = requests.request("GET", url, headers=headers, params=querystring)
getsummary = response.json()

with open("getsummary.json", "w") as f:
    f.write(json.dumps(getsummary, indent= 4))




